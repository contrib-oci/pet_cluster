resource oci_bastion_bastion bastion_petcluster {
  bastion_type = "STANDARD"
  client_cidr_block_allow_list = [
  "172.16.0.0/24", var.my_internet_ip,
  ]
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "luiz.fuzaro@gmail.com"
    "Oracle-Tags.CreatedOn" = "2021-12-30T12:45:16.882Z"
  }
  freeform_tags = {
  }
  max_session_ttl_in_seconds = "10800"
  name                       = "bastionpetcluster"
  #phone_book_entry = <<Optional value not found in discovery>>
  static_jump_host_ip_addresses = [
  ]
  target_subnet_id = oci_core_subnet.export_subnet-pet_cluster.id
}
