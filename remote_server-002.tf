resource "null_resource" "server-002" {
    depends_on = [oci_bastion_session.session-server_002]
      connection {
    type = "ssh"
    # The default username for our AMI
    user        = "opc"
    host        = oci_core_instance.server-002.private_ip
    private_key = file("~/.ssh/bastion-id_rsa")
    # The connection will use the local SSH agent for authentication.
    bastion_host        = "host.bastion.${var.region}.oci.oraclecloud.com" 
    bastion_user        = oci_bastion_session.session-server_002.id
    bastion_private_key = file("~/.ssh/bastion-id_rsa") 
  }
  provisioner "file" {
    source      = "files"
    destination = "/home/opc"
  }
  provisioner "remote-exec" {
      inline = [
      "sudo yum -y update",
      "sudo systemctl stop firewalld",
      "sudo systemctl disable firewalld",
      "sudo sed -i 's/=enforcing/=disabled/' /etc/selinux/config",
      "sudo yum config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo sed -i 's/$$releasever/8/g' /etc/yum.repos.d/docker-ce.repo",
      "sudo yum install -y docker-ce.aarch64",
      "sudo systemctl enable docker",
      "sudo yum install -y bind",
      "sudo cp files/dns/named.conf /etc/",  
      "sudo cp files/dns/consul.conf /etc/named/",  
      "sudo systemctl enable named",
      "wget https://releases.hashicorp.com/nomad/1.2.6/nomad_1.2.6_linux_arm64.zip",
      "unzip nomad_1.2.6_linux_arm64.zip",
      "sudo chown root:root nomad",
      "sudo mv nomad /usr/local/bin/",
      "sudo mkdir --parents /opt/nomad",
      "sudo useradd --system --home /etc/nomad.d --shell /bin/false nomad",
      "sudo mkdir --parents /etc/nomad.d",
      "sudo cp files/nomad/nomad.hcl /etc/nomad.d/",  
      "sudo cp files/nomad/server.hcl /etc/nomad.d/",  
      "sudo chown -R nomad:nomad /opt/nomad",
      "sudo chown -R nomad:nomad /etc/nomad.d",
      "sudo chmod 700 /etc/nomad.d",
      "sudo cp files/nomad.service /usr/lib/systemd/system/nomad.service",  
      "sudo systemctl enable nomad",
      "nomad -autocomplete-install",
      "complete -C /usr/local/bin/nomad nomad",
      "wget https://releases.hashicorp.com/consul/1.10.3/consul_1.10.3_linux_arm64.zip",
      "unzip consul_1.10.3_linux_arm64.zip",
      "sudo chown root:root consul",
      "sudo mv consul /usr/local/bin/",
      "sudo useradd --system --home /etc/consul.d --shell /bin/false consul",
      "sudo mkdir --parents /opt/consul",
      "sudo chown --recursive consul:consul /opt/consul",
      "sudo mkdir --parents /etc/consul.d",  
      "sudo cp files/consul/consul.hcl /etc/consul.d/",  
      "sudo cp files/consul/server.hcl /etc/consul.d/",  
      "sudo chown --recursive consul:consul /etc/consul.d",  
      "sudo chmod 640 /etc/consul.d/*.hcl",  
      "sudo cp files/consul.service /usr/lib/systemd/system/consul.service",  
      "sudo systemctl enable consul",
      "echo 'export NOMAD_ADDR=http://server-002:4646' >> /home/opc/.bashrc",
      "sudo shutdown -r +1"
    ]
  }
}
