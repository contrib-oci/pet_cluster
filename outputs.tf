output "server_001_instance_public_ip" {
  description = "Public IP address of the server_001 instance"
  value       = oci_core_instance.server-001.public_ip
}
output "server_002_instance_public_ip" {
  description = "Public IP address of the server_002 instance"
  value       = oci_core_instance.server-002.public_ip
}
output "server_003_instance_public_ip" {
  description = "Public IP address of the server_003 instance"
  value       = oci_core_instance.server-003.public_ip
}
output "server_004_instance_public_ip" {
  description = "Public IP address of the server_004 instance"
  value       = oci_core_instance.server-004.public_ip
}