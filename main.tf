##  Este script Terraform faz a configuração de um cluster (PetCluster) utilizando o tier Always Free da OCI (Oracle Cloud Infraestructure) para validação de conceitos e configurações. 
##  Enfim, gostei tanto de fazer isto, que acredito estar em um estágio estável e interessante para ser compartilhado e com contribuições, evoluir ....
##  

resource oci_core_instance server-001 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Management Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Custom Logs Monitoring"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Bastion"
    }
  }
  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-001"
    freeform_tags = {
    }
    hostname_label = "server-001"
    nsg_ids = [
    ]
    private_ip             = var.server_001_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-001"
  extended_metadata = {
  }
  #fault_domain = "FAULT-DOMAIN-1"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.srv_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"
  }

resource oci_core_instance server-002 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Bastion"
    }
  }

  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-002"
    freeform_tags = {
    }
    hostname_label = "server-002"
    nsg_ids = [
    ]
    private_ip             = var.server_002_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-002"
  extended_metadata = {
  }
  #fault_domain = "FAULT-DOMAIN-2"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.srv_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"

}

resource oci_core_instance server-003 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Bastion"
    }
  }

  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-003"
    freeform_tags = {
    }
    hostname_label = "server-003"
    nsg_ids = [
    ]
    private_ip             = var.server_003_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-003"
  extended_metadata = {
  }
#  fault_domain = "FAULT-DOMAIN-3"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.cli_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"

}

resource oci_core_instance server-004 {
  agent_config {
    are_all_plugins_disabled = "false"
    is_management_disabled   = "false"
    is_monitoring_disabled   = "false"
    plugins_config {
      desired_state = "DISABLED"
      name          = "Vulnerability Scanning"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "OS Management Service Agent"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Run Command"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Compute Instance Monitoring"
    }
    plugins_config {
      desired_state = "DISABLED"
      name          = "Block Volume Management"
    }
    plugins_config {
      desired_state = "ENABLED"
      name          = "Bastion"
    }
  }
  #async = <<Optional value not found in discovery>>
  availability_config {
    #is_live_migration_preferred = <<Optional value not found in discovery>>
    recovery_action = "RESTORE_INSTANCE"
  }
  availability_domain = var.oci_identity_availability_domain
  #capacity_reservation_id = <<Optional value not found in discovery>>
  compartment_id = var.compartment_ocid
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    assign_private_dns_record = "true"
    defined_tags = {
      "Oracle-Tags.CreatedBy" = "PetCluster"
    }
    display_name = "server-004"
    freeform_tags = {
    }
    hostname_label = "server-004"
    nsg_ids = [
    ]
    private_ip             = var.server_004_priv_ip
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.export_subnet-pet_cluster.id
    #vlan_id = <<Optional value not found in discovery>>
  }
  #dedicated_vm_host_id = <<Optional value not found in discovery>>
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "server-004"
  extended_metadata = {
  }
  #fault_domain = "FAULT-DOMAIN-1"
  freeform_tags = {
  }
  instance_options {
    are_legacy_imds_endpoints_disabled = "false"
  }
  #ipxe_script = <<Optional value not found in discovery>>
  #is_pv_encryption_in_transit_enabled = <<Optional value not found in discovery>>
  launch_options {
    boot_volume_type                    = "PARAVIRTUALIZED"
    firmware                            = "UEFI_64"
    is_consistent_volume_naming_enabled = "true"
    is_pv_encryption_in_transit_enabled = "true"
    network_type                        = "PARAVIRTUALIZED"
    remote_data_volume_type             = "PARAVIRTUALIZED"
  }
  metadata = {
    "ssh_authorized_keys" = var.ssh_public_key
  }
  #preserve_boot_volume = <<Optional value not found in discovery>>
  shape = var.srv_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.cli_mem
    ocpus                     = "1"
  }
  source_details {
    #boot_volume_size_in_gbs = <<Optional value not found in discovery>>
    #kms_key_id = <<Optional value not found in discovery>>
    source_id   = var.srv_source_image_id
    source_type = "image"
  }
  state = "RUNNING"

}

resource oci_core_internet_gateway export_Internet-Gateway-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "Internet Gateway vcn-pet_cluster"
  enabled      = "true"
  freeform_tags = {
  }
  vcn_id = oci_core_vcn.export_vcn-pet_cluster.id
}

#resource oci_bastion_bastion bastion_petcluster {
#  bastion_type = "STANDARD"
#  client_cidr_block_allow_list = [
#  "172.16.0.0/24", "177.205.119.237/32",
#  ]
#  compartment_id = var.compartment_ocid
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "luiz.fuzaro@gmail.com"
#    "Oracle-Tags.CreatedOn" = "2021-12-30T12:45:16.882Z"
#  }
#  freeform_tags = {
#  }
#  max_session_ttl_in_seconds = "10800"
#  name                       = "bastionpetcluster"
#  #phone_book_entry = <<Optional value not found in discovery>>
#  static_jump_host_ip_addresses = [
#  ]
#  target_subnet_id = oci_core_subnet.export_subnet-pet_cluster.id
#}

#resource oci_bastion_session session-server_001 {
#  bastion_id   = oci_bastion_bastion.bastion_petcluster.id
#  display_name = "session-server_001"
#  key_details {
#    public_key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Exlobj+OFWJrZiEGl4NMlu5I+7vj53i2KN6x0/8OQ+RUSV8dBFxnLuG0mjxW6pPdFBFjIdyXDP3GHghnQSnh29YpwsX6/ZDOvTMFGLNU6tvmq/ieMPkDYzxnmjYu01H4zFV7kikK2+Q5WtaDt7IHcbFwSjf/hD7pBqZl8ashOJB04B0RFsKe5GwocSHm5DExK5nWtbJ0SX5MDcANcsEJ9G3qLqpM0w2wAV7HaHkG6bb3h/z63QxHFgEOza2YIwTdEXnaY+QFBnxT1VncvpvrLcOzV/jWz0T3nqSPf67zNI0cEtaEbFhHZAOzCKUwKMq2d/CHJlu6+3r4JNRz/3FB luiz@fzmacbookpro\n"
#  }
#  key_type               = "PUB"
#  session_ttl_in_seconds = "10800"
#  target_resource_details {
#    session_type                               = "MANAGED_SSH"
#    target_resource_id                         = oci_core_instance.server-001.id
#    target_resource_operating_system_user_name = "opc"
#    target_resource_port                       = "22"
#    target_resource_private_ip_address         = "172.16.0.101"
#  }
#}

#resource oci_bastion_session session-server_002 {
#  bastion_id   = oci_bastion_bastion.bastion_petcluster.id
#  display_name = "session-server_002"
#  key_details {
#    public_key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Exlobj+OFWJrZiEGl4NMlu5I+7vj53i2KN6x0/8OQ+RUSV8dBFxnLuG0mjxW6pPdFBFjIdyXDP3GHghnQSnh29YpwsX6/ZDOvTMFGLNU6tvmq/ieMPkDYzxnmjYu01H4zFV7kikK2+Q5WtaDt7IHcbFwSjf/hD7pBqZl8ashOJB04B0RFsKe5GwocSHm5DExK5nWtbJ0SX5MDcANcsEJ9G3qLqpM0w2wAV7HaHkG6bb3h/z63QxHFgEOza2YIwTdEXnaY+QFBnxT1VncvpvrLcOzV/jWz0T3nqSPf67zNI0cEtaEbFhHZAOzCKUwKMq2d/CHJlu6+3r4JNRz/3FB luiz@fzmacbookpro\n"
#  }
#  key_type               = "PUB"
#  session_ttl_in_seconds = "10800"
#  target_resource_details {
#    session_type                               = "MANAGED_SSH"
#    target_resource_id                         = oci_core_instance.server-002.id
#    target_resource_operating_system_user_name = "opc"
#    target_resource_port                       = "22"
#    target_resource_private_ip_address         = "172.16.0.102"
#  }
#}

#resource oci_bastion_session session-server_003 {
#  bastion_id   = oci_bastion_bastion.bastion_petcluster.id
#  display_name = "session-server_003"
#  key_details {
#    public_key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Exlobj+OFWJrZiEGl4NMlu5I+7vj53i2KN6x0/8OQ+RUSV8dBFxnLuG0mjxW6pPdFBFjIdyXDP3GHghnQSnh29YpwsX6/ZDOvTMFGLNU6tvmq/ieMPkDYzxnmjYu01H4zFV7kikK2+Q5WtaDt7IHcbFwSjf/hD7pBqZl8ashOJB04B0RFsKe5GwocSHm5DExK5nWtbJ0SX5MDcANcsEJ9G3qLqpM0w2wAV7HaHkG6bb3h/z63QxHFgEOza2YIwTdEXnaY+QFBnxT1VncvpvrLcOzV/jWz0T3nqSPf67zNI0cEtaEbFhHZAOzCKUwKMq2d/CHJlu6+3r4JNRz/3FB luiz@fzmacbookpro\n"
#  }
#  key_type               = "PUB"
#  session_ttl_in_seconds = "10800"
#  target_resource_details {
#    session_type                               = "MANAGED_SSH"
#    target_resource_id                         = oci_core_instance.server-003.id
#    target_resource_operating_system_user_name = "opc"
#    target_resource_port                       = "22"
#    target_resource_private_ip_address         = "172.16.0.103"
#  }
#}

#resource oci_bastion_session session-server_004 {
#  bastion_id   = oci_bastion_bastion.bastion_petcluster.id
#  display_name = "session-server_004"
#  key_details {
#    public_key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Exlobj+OFWJrZiEGl4NMlu5I+7vj53i2KN6x0/8OQ+RUSV8dBFxnLuG0mjxW6pPdFBFjIdyXDP3GHghnQSnh29YpwsX6/ZDOvTMFGLNU6tvmq/ieMPkDYzxnmjYu01H4zFV7kikK2+Q5WtaDt7IHcbFwSjf/hD7pBqZl8ashOJB04B0RFsKe5GwocSHm5DExK5nWtbJ0SX5MDcANcsEJ9G3qLqpM0w2wAV7HaHkG6bb3h/z63QxHFgEOza2YIwTdEXnaY+QFBnxT1VncvpvrLcOzV/jWz0T3nqSPf67zNI0cEtaEbFhHZAOzCKUwKMq2d/CHJlu6+3r4JNRz/3FB luiz@fzmacbookpro\n"
#  }
#  key_type               = "PUB"
#  session_ttl_in_seconds = "10800"
#  target_resource_details {
#    session_type                               = "MANAGED_SSH"
#    target_resource_id                         = oci_core_instance.server-004.id
#    target_resource_operating_system_user_name = "opc"
#    target_resource_port                       = "22"
#    target_resource_private_ip_address         = "172.16.0.104"
#  }
#}
resource oci_core_subnet export_subnet-pet_cluster {
  #availability_domain = <<Optional value not found in discovery>>
  cidr_block     = "172.16.0.0/24"
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  dhcp_options_id = oci_core_vcn.export_vcn-pet_cluster.default_dhcp_options_id
  display_name    = "subnet-pet_cluster"
  dns_label       = "subnet11052106"
  freeform_tags = {
  }
  #ipv6cidr_block = <<Optional value not found in discovery>>
  prohibit_internet_ingress  = "false"
  prohibit_public_ip_on_vnic = "false"
  route_table_id             = oci_core_vcn.export_vcn-pet_cluster.default_route_table_id
  security_list_ids = [
    oci_core_vcn.export_vcn-pet_cluster.default_security_list_id,
  ]
  vcn_id = oci_core_vcn.export_vcn-pet_cluster.id
}

#resource oci_core_private_ip export_server-001 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-001"
#  freeform_tags = {
#  }
#  hostname_label = "server-001"
#  ip_address     = var.server_001_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljroy2wx3st2z34ooqmazmlhu5y6d3ctcmpxx4oi3wpwjcicx42ol5a"
#}
#
#resource oci_core_private_ip export_server-002 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-002"
#  freeform_tags = {
#  }
#  hostname_label = "server-002"
#  ip_address     = var.server_002_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljth2azwr5jvi6axmlpkjactfwxeuwp4hblnrcooq72w4lhcebv237q"
#}
#
#resource oci_core_private_ip export_server-003 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-003"
#  freeform_tags = {
#  }
#  hostname_label = "server-003"
#  ip_address     = var.server_003_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljtgdctfppcewdjo55oiv2efxvu722ylljqn6yylcxnbqvqta4kjjjq"
#}

#resource oci_core_private_ip export_server-004 {
#  defined_tags = {
#    "Oracle-Tags.CreatedBy" = "PetCluster"
#  }
#  display_name = "server-004"
#  freeform_tags = {
#  }
#  hostname_label = "server-004"
#  ip_address     = var.server_004_priv_ip
#  #vlan_id = <<Optional value not found in discovery>>
#  #vnic_id = "ocid1.vnic.oc1.iad.abuwcljswkt72jf5xkqkbcapjjdfqqlslpkpqri3b2uiw4pmko4lhhv2mwlq"
#}

resource oci_core_vcn export_vcn-pet_cluster {
  #cidr_block = <<Optional value not found in discovery>>
  cidr_blocks = [
    "172.16.0.0/16",
  ]
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "vcn-pet_cluster"
  dns_label    = "petcluster"
  freeform_tags = {
  }
 is_ipv6enabled = false
}
resource oci_core_default_dhcp_options export_Default-DHCP-Options-for-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "buscajuris"
  }
  display_name     = "Default DHCP Options for vcn-pet_cluster"
  domain_name_type = "CUSTOM_DOMAIN"
  freeform_tags = {
  }
  manage_default_resource_id = oci_core_vcn.export_vcn-pet_cluster.default_dhcp_options_id
  options {
    custom_dns_servers = [  "127.0.0.1", "169.254.169.254",
    ]
    #search_domain_names = <<Optional value not found in discovery>>
    server_type = "CustomDnsServer"
    type        = "DomainNameServer"
  }
  options {
    #custom_dns_servers = <<Optional value not found in discovery>>
    search_domain_names = [
      "petcluster.oraclevcn.com",
    ]
    #server_type = <<Optional value not found in discovery>>
    type = "SearchDomain"
  }
}

resource oci_core_default_route_table export_Default-Route-Table-for-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "Default Route Table for vcn-pet_cluster"
  freeform_tags = {
  }
  manage_default_resource_id = oci_core_vcn.export_vcn-pet_cluster.default_route_table_id
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.export_Internet-Gateway-vcn-pet_cluster.id
  }
}

resource oci_core_default_security_list export_Default-Security-List-for-vcn-pet_cluster {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
  }
  display_name = "Default Security List for vcn-pet_cluster"
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = "0.0.0.0/0"
    destination_type = "CIDR_BLOCK"
    #icmp_options = <<Optional value not found in discovery>>
    protocol  = "all"
    stateless = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  freeform_tags = {
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    icmp_options {
      code = "4"
      type = "3"
    }
    protocol    = "1"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    icmp_options {
      code = "-1"
      type = "3"
    }
    protocol    = "1"
    source      = "172.16.0.0/16"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "4647"
      min = "4647"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "4648"
      min = "4648"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "4648"
      min = "4648"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8500"
      min = "8500"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8600"
      min = "8600"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "8600"
      min = "8600"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "51820"
      min = "51820"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }

  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "17"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    #tcp_options = <<Optional value not found in discovery>>
    udp_options {
      max = "8302"
      min = "8301"
      #source_port_range = <<Optional value not found in discovery>>
    }
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8302"
      min = "8300"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.my_internet_ip
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8500"
      min = "8500"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.my_internet_ip
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "4646"
      min = "4646"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "32000"
      min = "20000"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "172.16.0.0/24"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "9999"
      min = "9998"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = var.my_internet_ip
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "9999"
      min = "9998"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
}

ingress_security_rules {
    #description = <<Optional value not found in discovery>>
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = "0.0.0.0/0"
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "80"
      min = "80"
      #source_port_range = <<Optional value not found in discovery>>
    }
    #udp_options = <<Optional value not found in discovery>>
  }
  manage_default_resource_id = oci_core_vcn.export_vcn-pet_cluster.default_security_list_id
}
## This configuration was generated by terraform-provider-oci

resource oci_load_balancer_load_balancer export_lb_ocijuris {
  compartment_id = var.compartment_ocid
  defined_tags = {
    "Oracle-Tags.CreatedBy" = "PetCluster"
    "Oracle-Tags.CreatedOn" = "2021-12-05T02:27:33.152Z"
  }
  display_name = "lb_ocijuris"
  freeform_tags = {
  }
  ip_mode    = "IPV4"
  is_private = "false"
  network_security_group_ids = [
  ]
  reserved_ips  {
    id = var.load_balancer_reserved_ips_id
  }
  shape = "flexible"
  shape_details {
    maximum_bandwidth_in_mbps = "10"
    minimum_bandwidth_in_mbps = "10"
  }
  subnet_ids = [
    oci_core_subnet.export_subnet-pet_cluster.id,
    ]
}

resource oci_load_balancer_backend_set export_bs_lb_2021-1204-2324 {
  health_checker {
    interval_ms         = "10000"
    port                = "9998"
    protocol            = "HTTP"
    response_body_regex = ""
    retries             = "3"
    return_code         = "200"
    timeout_in_millis   = "3000"
    url_path            = "/health"
  }
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  name             = "bs_lb_2021-1204-2324"
  policy           = "ROUND_ROBIN"
}

resource oci_load_balancer_backend export_172-16-0-104-9999 {
  backendset_name  = oci_load_balancer_backend_set.export_bs_lb_2021-1204-2324.name
  backup           = "false"
  drain            = "false"
  ip_address       = "172.16.0.104"
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  offline          = "false"
  port             = "9999"
  weight           = "1"
}

resource oci_load_balancer_backend export_172-16-0-103-9999 {
  backendset_name  = oci_load_balancer_backend_set.export_bs_lb_2021-1204-2324.name
  backup           = "false"
  drain            = "false"
  ip_address       = "172.16.0.103"
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  offline          = "false"
  port             = "9999"
  weight           = "1"
}

resource oci_load_balancer_listener export_lb_ocijuris_listener_lb_2021-1204-2324 {
  connection_configuration {
    backend_tcp_proxy_protocol_version = "0"
    idle_timeout_in_seconds            = "60"
  }
  default_backend_set_name = oci_load_balancer_backend_set.export_bs_lb_2021-1204-2324.name
  hostname_names = [
  ]
  load_balancer_id = oci_load_balancer_load_balancer.export_lb_ocijuris.id
  name             = "listener_lb_2021-1204-2324"
  #path_route_set_name = <<Optional value not found in discovery>>
  port     = "80"
  protocol = "HTTP"
  #routing_policy_name = <<Optional value not found in discovery>>
  rule_set_names = [
  ]
}

