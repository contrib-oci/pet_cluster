## Pet Cluster [OCI](https://www.oracle.com/cloud/free/) - [Nomad](https://www.nomadproject.io/) 

Este script [Terraform](https://www.terraform.io/) faz a configuração de um cluster (PetCluster) utilizando o tier Always Free da OCI (Oracle Cloud Infraestructure) para validação de conceitos e configurações. 
  Enfim, gostei tanto de fazer isto, que acredito estar em um estágio estável e interessante para ser compartilhado e com contribuições, evoluir. Afinal, esta é uma primeira produção integrando disciplinas na qual espero também evoluir como : 

      - OCI
      - Terraform
      - Nomad
  
  
##  O RACIONAL (+/-) 
  
 Durante alguns anos tenho utilizado tecnologia de containers [Docker](https://www.docker.com/) com orquestrador [Rancher](https://rancher.com/).
 Entretanto, o Rancher 1.6 que utilizava foi descontinuado e as versões ficaram sem suporte e deteriorando nos aspectos de segurança e compatibilidade, em consequência acabei ficando sem uma alternativa tecnológica para evolução desta plataforma.

 -Mas, e [Kubernetes](https://kubernetes.io/) não resolveria?
 
 -Sim, o Rancher 2.0 2.5/6, também tentei e usei sim, passei 2 anos aprendendo Kubernetes, Helm, K3s/K3d, muito legal, mas, não!

 Tudo é muito bom se vc tiver só de escolher o tamanho dos servidores do seu cluster, pegar as credenciais e começar a derrubar container dentro e é claro uns $$$$$.
 
 Mas, particularmente, tenho uns desafios diferentes! 
 As minhas demandas não justificam tanto tamanho esforço e dispêndio de recursos computacionais, resolvi salvar umas àrvores!

 Crio soluções de plataforma que trazem cenários diferentes, persistências, containers Long Running statefull afinal nem tudo é CI/CD e nem todo mundo precisa da mesma solução, nem que seja do Google!
 
 
 E olha só, ainda em 2021 um afunilamento tecnológico pressiona todo mundo para Kubernetes. Veja [aqui](https://www.cloudzero.com/blog/container-orchestration) e [aqui](https://devopscube.com/docker-container-clustering-tools/). Eu que estou enxergando mal ou é tudo parecido com Açaí... Com Banana, Com Guaraná, com farelo, Com granola, com Peixe, pouquísimas indicações apresentam realmente uma alternativa real.

 O que se move nas plataformas são os dados. Depois que a arquitetura é montada, são dados e códigos que circulam em um ambiente mais estático, porque inventaram o termo PAAS, heim..... Sacou?

 Procurei bastante, até pensei em trocar a ferramenta de busca, pois não aparece uma resposta diferente para orquestração de containers nas pesquisas.

Parece que tudo é Açaí mesmo!

Bem por aí......vagando.... acabei topando com o Nomad e apesar de ser bem diferente com uma curva de aprendizado mais longa, me pareceu adequado. Aliás, vale ressaltar que é de bom que se aprenda a interagir com o ecosistema da Hashicorp para obter melhores resultados.
Bem, é isto aí.

 Mais informações em https://www.nomadproject.io/ 

## Objetivo

 Ao final voce terá um cluster Nomad e [Consul](https://www.consul.io/) da [Hashicorp](https://www.hashicorp.com/) com um load balancer [Fabio](https://fabiolb.net/) fronteando o cluster em 4 servidores com arquitetura [arm64](https://jumpcloud.com/blog/why-should-you-use-arm64) com 2 servers e 2 clients.
 A arquitetura preconizada é 3 servers por causa do [Raft Consensus](http://thesecretlivesofdata.com/raft/), blá, blá, bla, mas funciona com dois, e muito bem até com um server.

## Principais características do Cluster

### Sistema Operacional Oracle Linux 8 (Quem diria...)
     - SELINUX Desabilitado
     - Pacotes Instalados :
       - Docker
       - Bind (explico depois porque)
     - Binários instalados
       - Nomad
       - Consul
       - CNI (Container Network Interface)

### Configuração dos Servidores :

      - 1 OCPU Ampere arquitetura arm64
      - 4/8 Gigabytes de memória RAM (4GB Servers e 8GB Clientes)
      - 50 GB de Block Storage como dispositivo raiz
      - 01 IP Público
      - 01 IP Privado

### Ambiente de rede :

      - 1 VCN 172.16.0.0/16 
      - 1 Subnet Publica 172.16.0.0/24 [172.16.0.0.0-255]
      - 1 IGR Internet Gateway Router (NAT) para a rede 172.16.0.0/24
      - 1 Router Table com uma rota default para o IGR
      - 1 DHCP Server configurado para rede 172.16.0.0/24 como CustomDnsServer e dois DNS's 127.0.0.1, 169.254.169.254 (um para depois e um para instalação, nesta ordem ;) )
      - 1 Load Balancer
      - 2 Backeends apontando para os Client's (executores de container)
        - ATENÇÃO : O Load Balancer Requer um Reserved IP prévio OCID --> "var.load_balancer_reserved_ips_id"
#### Security List
   Varias regras com permissões de acesso sendo que as mais relevantes por hora:

      - EGRESS
       Tráfego liberado para saida via IGR (internet Gateway Router)

      - INGRESS
       Portas : 
             22 (ssh) Permitido acesso externo (Internet)
             80 (http)     "               "         "
             Algumas outras portas permitidas para o conteúdo da variável **var.my_internet_ip** seu ip de casa/empresa)
       Protocolo
             ICMP Ping

####    - TRÁFEGO INTERNO (172.16.0.0/24)
      Comunicação entre servidores habilitada pontualmente para os serviços necessários 
 
#### Arquivos Auxiliares
Os arquivos e diretórios marcados (*) são utilizados na instalação.
Os exemplos de jobs e volumes estão nos respectivos diretórios. 


```
files
├── consul (*)
│   ├── client.hcl
│   ├── consul.hcl
│   ├── server.hcl
│   └── terraform.tfstate
├── consul.service (*)
├── dns (*)
│   ├── consul.conf
│   └── named.conf
├── jobs (exemplo)
│   ├── elastic.nomad
│   ├── example-b.nomad
│   ├── example.nomad
│   ├── fabio.nomad
│   ├── mongodb.nomad
│   ├── nginx.nomad
│   ├── postgresSQL.nomad
│   └── webserver.nomad
├── nomad (*)
│   ├── anonymous.policy.hcl
│   ├── client.hcl
│   ├── nomad.hcl
│   └── server.hcl
├── nomad.client.service (*)
├── nomad.service (*)
└── volumes (exemplo)
    ├── v_d_mongodb.hcl
    └── v_d_postgres-data.hcl
```


### Requisitos

      - A conta na OCI
        - Credenciais de API - Identity & Security > Users -> API Key -> ViewConfigFile
        - Criar um Compartimento (OCID)
        - Criar um Reserved Public IP (OCID)
        - OCI CLI (Opcional, mas, é melhor ter.) 
      - Seu ip de acesso a internet [ipv4](https://whatismyipaddress.com/)  
      - Preencher o arquivo vars.tf
      - Terraform instalado 
        - Variáveis de ambiente configuradas :
          - export TF_VAR_tenancy_ocid=
          - export TF_VAR_user_ocid=
          - export TF_VAR_fingerprint=
          - export TF_VAR_private_key_path=
          - export TF_VAR_region=
        - terraform
          - init
          - validate
          - plan
          - apply
        - 15 minutos

#### Primeiro e Segundo Job 
  - Acessar o servidor 
    - ssh opc@ip.publico.do.server-001
      - cd files/job
      - nomad plan fabio.nomad (Load Balancer, Job tipo System)
      - nomad run fabio.nomad
      - nomad plan nginx.nomad
      - nomad run nginx.nomad
  - Se tudo deu certo vc vai acessar no Browser
    - NGINX landing page :  http://ip.publico.reserved/nginx
    - Nomad ui : http://ip.publico.do.server-001:4646 
    - Consul ui : http://ip.publico.do.server-001:8500
    - Fabio Load Balancer (02 instancias) :
      -  http://ip.publico.do.server-003:9998
      -  http://ip.publico.do.server-004:9998
  - Se vc conseguiu acessar toadas as páginas, é sinal que está tudo funcionando e vc tem um PetCluster de Nomad.
    - "Qualquer tecnologia suficientemente avançada é indistinguível da mágica." 
    - Isto não é mágica! É tecnologia avançada!
 :wq 