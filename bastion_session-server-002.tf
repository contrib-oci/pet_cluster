resource "time_sleep" "wait_300_seconds-002" {
        depends_on = [oci_core_instance.server-002]
  create_duration = "300s"
}

# This resource will create (at least) 30 seconds after null_resource.previous
resource "null_resource" "next-002" {
  depends_on = [time_sleep.wait_300_seconds-002]
}
resource oci_bastion_session session-server_002 {
        depends_on = [null_resource.next-002]
  bastion_id   = oci_bastion_bastion.bastion_petcluster.id
  display_name = "session-server_002"
  key_details {
    public_key_content = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9Exlobj+OFWJrZiEGl4NMlu5I+7vj53i2KN6x0/8OQ+RUSV8dBFxnLuG0mjxW6pPdFBFjIdyXDP3GHghnQSnh29YpwsX6/ZDOvTMFGLNU6tvmq/ieMPkDYzxnmjYu01H4zFV7kikK2+Q5WtaDt7IHcbFwSjf/hD7pBqZl8ashOJB04B0RFsKe5GwocSHm5DExK5nWtbJ0SX5MDcANcsEJ9G3qLqpM0w2wAV7HaHkG6bb3h/z63QxHFgEOza2YIwTdEXnaY+QFBnxT1VncvpvrLcOzV/jWz0T3nqSPf67zNI0cEtaEbFhHZAOzCKUwKMq2d/CHJlu6+3r4JNRz/3FB luiz@fzmacbookpro\n"
  }
  key_type               = "PUB"
  session_ttl_in_seconds = "10800"
  target_resource_details {
    session_type                               = "MANAGED_SSH"
    target_resource_id                         = oci_core_instance.server-002.id
    target_resource_operating_system_user_name = "opc"
    target_resource_port                       = "22"
    target_resource_private_ip_address         = "172.16.0.102"
  }
}
